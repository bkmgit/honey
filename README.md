# GitLab Shop Page with Stripe Checkout

This is an example of a client-only (server-free) Shop page that can be hosted on GitLab using Stripe Checkout.

You can see this repository running on [GitLab Pages](https://gitlab.com/pages) here: https://bkmgit.gitlab.io/honey/
<img src="./demo.gif" alt="A gif of the Checkout payment page rendering" align="center">

## Setup

- Create Stripe Account: https://dashboard.stripe.com/register
- Enable client-only checkout: https://dashboard.stripe.com/account/checkout/settings
- Create a one-time or recurring product in the Stripe Dashboard: https://dashboard.stripe.com/products
  - After creation click the "Use with checkout" button and copy the sku (sku_xxx) or plan (plan_xxx) ID
  - Paste the IDs into the button `data-sku-id`/`data-plan-id` attributes.
- Copy your publishable key from: https://dashboard.stripe.com/apikeys and set it as the value for `PUBLISHABLE_KEY` in the index.html file

## Run locally

Since these are all static assets you can view them locally by opening them in a webbrowser, e.g. on Linux command line

    firefox index.html

You should now be able to view the page in your browser file:///path/to/folder/public/index.html


## Go live

- Add `username.gitlab.io/honey` (replace username with your GitLab user name) to the domain whitelist in https://dashboard.stripe.com/account/checkout/settings
- Replace the test publishable key `PUBLISHABLE_KEY` in the index.html file with your pk\*live_xxx key which can be found here: https://dashboard.stripe.com/test/apikeys (!!!**NOTE**!!!: never paste in your secret key! For client-only Checkout only the publishable key is needed!)
- Commit the changes to the `master` branch and push them to GitLab.
- Done, you can now accept live payments on your GitLab pages \o/

## More Checkout Samples

- [checkout-one-time-payments](https://github.com/stripe-samples/checkout-one-time-payments)
- [checkout-single-subscription](https://github.com/stripe-samples/checkout-single-subscription)
- [checkout-subscription-and-add-on](https://github.com/stripe-samples/checkout-subscription-and-add-on)

## FAQ

Q: Why did you pick these frameworks?

A: We chose the most minimal framework to convey the key Stripe calls and concepts you need to understand. These demos are meant as an educational tool that helps you roadmap how to integrate Stripe within your own system independent of the framework.

Q: Can you show me how to build X?

A: We are always looking for new recipe ideas, please email dev-samples@stripe.com with your suggestion!

## Images

Taken from:

* https://commons.wikimedia.org/wiki/File:Honey_Jar_icon.svg

## Author(s)

- [@adreyfus-stripe](https://twitter.com/adrind)
- [@thorsten-stripe](https://twitter.com/thorwebdev)
